# ESignature

[![CI Status](http://img.shields.io/travis/HassanRazaKhalid/ESignature.svg?style=flat)](https://travis-ci.org/HassanRazaKhalid/ESignature)
[![Version](https://img.shields.io/cocoapods/v/ESignature.svg?style=flat)](http://cocoapods.org/pods/ESignature)
[![License](https://img.shields.io/cocoapods/l/ESignature.svg?style=flat)](http://cocoapods.org/pods/ESignature)
[![Platform](https://img.shields.io/cocoapods/p/ESignature.svg?style=flat)](http://cocoapods.org/pods/ESignature)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ESignature is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ESignature'
```

## Author

HassanRazaKhalid, evento.h.raza@ia.gov.ae

## License

ESignature is available under the MIT license. See the LICENSE file for more info.
