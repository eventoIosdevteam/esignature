//
//  ViewController.swift
//  ESignature
//
//  Created by HassanRazaKhalid on 10/18/2017.
//  Copyright (c) 2017 HassanRazaKhalid. All rights reserved.
//

import UIKit
import ESignature
import EPSignature

class ViewController: UIViewController {

    let handler = ESignatureMenuHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        handler.delegate = self
        handler.showMenu(title: "Signature Options", message: "Select Signature", signaturesTypes: [
            ESignatureMenuHandler.SignatureModel(title: "Camera", type: .hardCamera),
            ESignatureMenuHandler.SignatureModel(title: "Gallery", type: .hardGallery),
            ESignatureMenuHandler.SignatureModel(title: "Stored Signature", type: .stored),
            ESignatureMenuHandler.SignatureModel(title: "Digital", type: .digital),
            ], controller: self)
//        let signature = ESig
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: ESignatureMenuHandlerProtocol {
    func signatureSelectedAtURL(url: URL?, type: ESignatureMenuHandler.SignatureType) {
        
        print(url)
    }
    
    
    func configureDigitalController(signatureVC: EPSignatureViewController) {
        
        signatureVC.subtitleText = "I agree to the terms and conditions"
        signatureVC.title = "John Doe"
        
        let color = UIColor.green
        signatureVC.tintColor = color
    }
}
