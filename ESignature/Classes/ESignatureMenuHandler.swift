

import EPSignature

public protocol ESignatureMenuHandlerProtocol: class {
    
    func configureDigitalController(signatureVC: EPSignatureViewController)
    func signatureSelectedAtURL(url: URL?, type: ESignatureMenuHandler.SignatureType)
}

public class ESignatureMenuHandler: NSObject {
    
    public enum SignatureType {
        
        case none
        case stored
        case hardGallery
        case hardCamera
        case digital
    }
    
    public struct SignatureModel {
        let title: String
        let type: SignatureType
        
        public init(title: String, type: SignatureType) {
            self.title = title
            self.type = type
        }
    }
    public weak var delegate: ESignatureMenuHandlerProtocol?
    fileprivate var selectType: SignatureModel?
    
    public func showMenu(
        title: String?,
        message: String?,
        signaturesTypes: [SignatureModel],
        controller: UIViewController
        ) {
        
        
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: isIpad() ? .alert: .actionSheet)
        
        for sig in signaturesTypes {
            
            let style: UIAlertActionStyle = sig.type == .none ? .destructive : .default
            
            let action = UIAlertAction(title: sig.title, style: style, handler: {[weak self] (action) in
                self?.signatureOptionSelected(sender: sig, controller: controller)
            })
            actionSheet.addAction(action)
        }
        
        if isIpad()
        {
            actionSheet.isModalInPopover = true
            actionSheet.popoverPresentationController?.sourceView = controller.view
            actionSheet.popoverPresentationController?.sourceRect = controller.view.bounds
            controller.present(actionSheet, animated: true, completion: nil)
        } else {
            controller.show(actionSheet, sender: nil)
        }
        
    }
    
    func isIpad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    func signatureOptionSelected(sender: SignatureModel, controller: UIViewController) {
        
        selectType = sender
        switch sender.type {
        case .digital:
            showDigitalSignature(controller: controller)
        case .hardGallery:
            DocumentPickerController.showController(controller: controller, sourceType: .photoLibrary, customDelegate: self)
        case .hardCamera:
            DocumentPickerController.showController(controller: controller, sourceType: .camera, customDelegate: self)
        case .stored:
            delegate?.signatureSelectedAtURL(url: nil, type: sender.type)
        case .none:
            break
        }
    }
    
    private func showDigitalSignature(controller: UIViewController) {
        
        let signatureVC = EPSignatureViewController(signatureDelegate: self, showsDate: true, showsSaveSignatureOption: true)
        let nav = UINavigationController(rootViewController: signatureVC)
        delegate?.configureDigitalController(signatureVC: signatureVC)
        controller.show(nav, sender: nil)
    }
}

extension ESignatureMenuHandler: EPSignatureDelegate {
    
    public func epSignature(_ controller: EPSignatureViewController, didCancel error : NSError) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    public func epSignature(_ controller: EPSignatureViewController, didSign signatureImage : UIImage, boundingRect: CGRect) {
        
        let fileName = Date().currentDateString() + ".png"
        let path = signatureImage.saveToTmpDirectory(fileName: fileName)
        controller.dismiss(animated: true, completion: nil)
        
        guard let type = selectType else {
            return
        }
        delegate?.signatureSelectedAtURL(url: path, type: type.type)
    }
}

extension ESignatureMenuHandler: DocumentPickerProtocol {
    
    func documentDidSelected(controller: DocumentPickerController, path: URL?) {
        
        controller.dismiss(animated: true, completion: nil)
        guard let type = selectType else {
            return
        }
        delegate?.signatureSelectedAtURL(url: path, type: type.type)
    }
    
}

