//
//  DocumentPickerController.swift
//  EInspection
//
//  Created by HassanRazaKhalid on 4/27/17.
//  Copyright © 2017 IA. All rights reserved.
//

import UIKit
import Photos

protocol DocumentPickerProtocol: class {
    
    func documentDidSelected(controller: DocumentPickerController, path: URL?)
}

class DocumentPickerController: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    weak var customDelegate: DocumentPickerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("we have error")
            return
        }
        
        if picker.sourceType == .camera {
            
            let tmpData = UIImagePNGRepresentation(image)!
            let fileName = Date().currentDateString() + ".png"
            let finalFilePath = self.saveImageData(data: tmpData, fileName: fileName)
            self.customDelegate?.documentDidSelected(controller: self, path: finalFilePath)
        }
        else {
            
            guard let imageURL = info[UIImagePickerControllerReferenceURL] as? URL else {
                return
            }
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            guard let asset = result.firstObject else {
                return
            }
            let fileName = asset.value(forKey: "filename") as! String
            print(fileName)
            
            PHImageManager.default().requestImageData(for: asset, options: nil) { (data, value, orientation, dict) in
                
                guard let tmpData = data else {
                    return
                }
                let finalFilePath = self.saveImageData(data: tmpData, fileName: fileName)
                self.customDelegate?.documentDidSelected(controller: self, path: finalFilePath)
            }
        }
        // Just tmp File to access later stage since we are supporting single file selection for now
        //        let fileName = Date().currentDateString() + ".jpeg"//"tmp.png"
        //        let url = image.saveToTmpDirectory(fileName: fileName)
        //        self.customDelegate?.documentDidSelected(controller: self, path: url)
    }
    
    private func saveImageData(data: Data, fileName: String) -> URL {
        let finalFilePath = data.saveDataAtPath(filePath: UIImage.getImagePath(fileName: fileName))
        return finalFilePath
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.customDelegate?.documentDidSelected(controller: self, path: nil)
    }
}

extension DocumentPickerController {
    
    static func showController(
        controller: UIViewController,
        sourceType: UIImagePickerControllerSourceType,
        customDelegate: DocumentPickerProtocol?) {
        
        let docController = DocumentPickerController()
        docController.customDelegate = customDelegate
        docController.sourceType = sourceType
        controller.present(docController, animated: true, completion: nil)
    }
}

extension Date {
    
    func currentDateString() -> String {
        
        let df = DateFormatter()
        df.dateFormat = "yyyyMMddHHmmss"
        let tmpDateString = df.string(from: self)
        return tmpDateString
    }
}

extension Data {
    
    func saveDataAtPath(filePath: String) -> URL {
        FileManager.default.createFile(atPath: filePath, contents: self, attributes: nil)
        return URL(fileURLWithPath: filePath)
    }
}
