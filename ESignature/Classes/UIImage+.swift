//
//  UIImage+.swift
//  ESignature
//
//  Created by HassanRazaKhalid on 18/10/2017.
//

import Foundation

extension UIImage {
    
    func clearTempFolder(at path: String) {
        let fileManager = FileManager.default
        //        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: path)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: path + "/" + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    static func tempFilesPath() -> String {
        
        let fileManager = FileManager.default
        
        //        documentDirectory
        let paths = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString)
        
        var isDir: ObjCBool = false
        
        let imageFolderPath: NSString = paths.appendingPathComponent("Images") as NSString
        
        if  !fileManager.fileExists(atPath: imageFolderPath as String, isDirectory: &isDir) {
            do {
                let _ = try fileManager.createDirectory(at: URL(fileURLWithPath: imageFolderPath as String, isDirectory: true), withIntermediateDirectories: true, attributes: nil)
            }
            catch (_) {
            }
        }
        return imageFolderPath as String
    }
    
    static func getImagePath(fileName: String) -> String {
        
        let imageFolderPath = UIImage.tempFilesPath() as NSString
        let filePath = imageFolderPath.appendingPathComponent(fileName)
        return filePath
    }
    
    func saveToTmpDirectory(fileName: String, clearDir: Bool = true) -> URL {
        
        let filePath = UIImage.getImagePath(fileName: fileName)
        
        let imageData = UIImageJPEGRepresentation(self, 0.5)
        imageData?.saveDataAtPath(filePath: filePath)
        return URL(fileURLWithPath: filePath)
    }
}
